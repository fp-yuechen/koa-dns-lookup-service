#!/bin/bash

cwd=`pwd`

if [[ $1 = "-loc" ]]; then
    find . -name '*.go' | xargs wc -l | sort -n
    exit
fi

VER=0.1.0beta
ID=$(git rev-parse HEAD | cut -c1-7)

cd src

if [[ $1 = "-linux" ]]; then
    CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags "-X main.VERSION=$VER -X main.BuildID=$ID -w"
    exit
else
    go build -ldflags "-X main.VERSION=$VER -X main.BuildID=$ID -w" -o koa-agent
    #go build -gcflags "-N -l"
    #go build -race -v -ldflags "-X main.VERSION=$VER -X main.BuildID=$ID -w"
fi

mv koa-agent ../

#---------
# show ver
#---------
cd $cwd
./koa-agent -v
