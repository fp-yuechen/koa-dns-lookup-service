package agent

import (
	"fmt"
	"io"
	log "koa-agent/lib/logrus"
	"net/http"
	_ "net/http/pprof"
)

type Monitor struct {
	agent *KoaAgent
	quit  chan bool
}

func NewMonitor(agent *KoaAgent) (this *Monitor) {
	this = new(Monitor)
	this.agent = agent
	this.quit = make(chan bool)
	return
}

func (this *Monitor) httpHandler(w http.ResponseWriter, req *http.Request) {
	req.ParseForm()
	cmd := req.Form["cmd"][0]

	switch cmd {
	case "enable":
		this.agent.scheduler.flag = true
		log.Info("Redis scancer start working")
		io.WriteString(w, "OK")
	case "disable":
		this.agent.scheduler.flag = false
		log.Info("Redis scancer stop working")
		io.WriteString(w, "OK")
	}
}

func (this *Monitor) Run() {
	addr := fmt.Sprintf(":%d", this.agent.config.ListenPort)
	http.HandleFunc("/monitor", this.httpHandler)
	go http.ListenAndServe(addr, nil)
	log.Info("Monitor is runnning...")

	for {
		select {
		case <-this.quit:
			log.Info("Monitor stopped!")
			this.agent.wg.Done()
			return
		}
	}
}

func (this *Monitor) Stop() {
	close(this.quit)
}
