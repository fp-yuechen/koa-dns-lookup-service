package agent

import (
	"fmt"
	"github.com/garyburd/redigo/redis"
	log "koa-agent/lib/logrus"
	"os"
	"time"
	"io/ioutil"
)

type Scheduler struct {
	agent *KoaAgent
	flag  bool
	quit  chan bool
}

func NewScheduler(agent *KoaAgent) (this *Scheduler) {
	this = new(Scheduler)
	this.agent = agent
	this.quit = make(chan bool)
	this.flag = true
	return
}

func (this *Scheduler) Run() {
	duration, err := time.ParseDuration(this.agent.config.Interval)
	if err != nil {
		panic("Invalid interval string in config")
	}

	connTimeout, err := time.ParseDuration(this.agent.config.RedisConfig.ConnTimeout)
	if err != nil {
		panic("Invalid interval string in config")
	}

	readTimeout, err := time.ParseDuration(this.agent.config.RedisConfig.ConnTimeout)
	if err != nil {
		panic("Invalid interval string in config")
	}

	writeTimeout, err := time.ParseDuration(this.agent.config.RedisConfig.ConnTimeout)
	if err != nil {
		panic("Invalid interval string in config")
	}
	log.Info("Redis scaner is running...")

	address := fmt.Sprintf("%s:%d", this.agent.config.RedisConfig.Host, this.agent.config.RedisConfig.Port)

	ticker := time.NewTicker(duration)
	defer ticker.Stop()

	for {
		select {
		case <-ticker.C:
			func() {
				if this.flag == false {
					return
				}
				conn, err := redis.DialTimeout("tcp", address, connTimeout, readTimeout, writeTimeout)
				if err != nil {
					log.Errorf("Redis connect failed (%s): %s", address, err.Error())
					return
				}
				defer conn.Close()
				bytes, err := redis.Bytes(conn.Do("GET", this.agent.config.RedisKey))
				if err != nil {
					log.Errorf("Redid read failed: %s", err.Error())
					return
				}
				log.Debugf("Read from redis: %v", string(bytes))

				localFileName := this.agent.config.LocalFile
				contentBytes, err := ioutil.ReadFile(localFileName)

				if err != nil {
					log.Error(err.Error())
					return
				}

				if string(contentBytes) == string(bytes) {
					log.Debug("Nothing changed!")
					return
				}

				// 开始写入新的内容
				tmpFilePrefix := fmt.Sprintf("%d_", os.Getpid())
				tmpFile, err := ioutil.TempFile("", tmpFilePrefix)

				if err != nil {
					log.Error(err.Error())
					return
				}

				tmpFileName := tmpFile.Name()
				log.Debugf("Created tmp file: %s", tmpFileName)
				_, err = tmpFile.Write(bytes)

				if err != nil {
					tmpFile.Close()
					log.Error(err.Error())
					return
				}

				tmpFile.Close()
				err = os.Rename(tmpFileName, localFileName)

				if err != nil {
					log.Error(err.Error())
					return
				}

				log.Debug("Save changed!")
			}()
		case <-this.quit:
			log.Info("Redis scaner stopped!")
			this.agent.wg.Done()
			return
		}
	}
}

func (this *Scheduler) Stop() {
	close(this.quit)
}
