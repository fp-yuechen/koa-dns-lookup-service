package agent

import (
	"koa-agent/src/config"
	"sync"
	"os"
	"fmt"
	"syscall"
)

type KoaAgent struct {
	config *config.KoaConfig

	monitor   *Monitor
	scheduler *Scheduler

	wg *sync.WaitGroup
}

func New(config *config.KoaConfig) (this *KoaAgent) {
	this = new(KoaAgent)
	this.config = config
	this.scheduler = NewScheduler(this)
	this.monitor = NewMonitor(this)
	this.wg = new(sync.WaitGroup)
	return
}

func (this *KoaAgent) Serve() {
	this.lockPidFile()
	this.wg.Add(2)
	go this.scheduler.Run()
	go this.monitor.Run()
}

func (this *KoaAgent) Stop() {
	this.monitor.Stop()
	this.scheduler.Stop()
}

func (this *KoaAgent) Wait() {
	this.wg.Wait()
}

func (this *KoaAgent) lockPidFile() {
	pid := fmt.Sprintf("%d", os.Getpid())
	lockFile, err := os.OpenFile(this.config.PidFile, os.O_WRONLY | os.O_TRUNC | os.O_CREATE, 0660)
	if err != nil {
		panic("Can not open fid file")
	}
	_, err = lockFile.Write([]byte(pid))
	if err != nil {
		panic("Can not write fid file")
	}
	err = syscall.Flock(int(lockFile.Fd()), syscall.LOCK_EX | syscall.LOCK_NB)
	if err != nil {
		fmt.Println("Another instance is running...")
		os.Exit(1)
	}
}

func (this *KoaAgent) Clear() {
	os.Remove(this.config.PidFile)
}
