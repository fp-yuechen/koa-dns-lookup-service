package main

import (
	"flag"
	"fmt"
	log "koa-agent/lib/logrus"
	"koa-agent/lib/signal"
	"koa-agent/src/agent"
	"koa-agent/src/config"
	"os"
	"syscall"
)

var (
	BuildID = "unknown"
	VERSION = "unknown"

	options struct {
		configFile  string
		showVersion bool
	}

	koaAgent *agent.KoaAgent
)

func parseFlags() {
	flag.StringVar(&options.configFile, "conf", "conf/koa-agent.conf", "config file")
	flag.BoolVar(&options.showVersion, "v", false, "show version and exit")
	flag.Parse()
}

func shutdown() {
	koaAgent.Stop()
	koaAgent.Clear()
}

func clear() {
	koaAgent.Clear()
}

func init() {
	parseFlags()

	if options.showVersion {
		fmt.Println(VERSION)
		os.Exit(0)
	}

	_, err := os.Stat(options.configFile)
	if err != nil && os.IsNotExist(err) {
		panic("koa-agent.conf not found, please check your config!")
	}

	signal.RegisterHandler(func(sig os.Signal) {
		shutdown()
	}, syscall.SIGINT, syscall.SIGTERM)
}

func main() {
	koaConfig := config.New()
	koaConfig.LoadConfig(options.configFile)
	koaConfig.OpenLog()

	koaAgent = agent.New(koaConfig)
	koaAgent.Serve()
	log.Info("Koa-agent service startted!")
	koaAgent.Wait()
	log.Info("Koa-agent service stopped!")
	defer clear()
}
