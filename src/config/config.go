package config

import (
	"encoding/json"
	log "koa-agent/lib/logrus"
	"os"
)

type redisConfig struct {
	Host         string `json:"host"`
	Port         int    `json:"port"`
	ConnTimeout  string `json:"conn_timeout"`
	ReadTimeout  string `json:"read_timeout"`
	WriteTimeout string `json:"write_timeout"`
}

type KoaConfig struct {
	ListenPort int    `json:"listen_port"`
	LogLevel   string `json:"log_level"`
	LogFile    string `json:"log_file"`
	LogFormat  string `json:"log_format"`
	PidFile    string `json:"pid_file"`
	RedisKey   string `json:"key"`
	LocalFile  string `json:"local_file"`
	Interval   string `json:"interval"`

	RedisConfig *redisConfig `json:"redis"`
}

func New() (this *KoaConfig) {
	this = new(KoaConfig)
	return
}

func (this *KoaConfig) LoadConfig(configFilePath string) {
	config, err := os.Open(configFilePath)
	defer config.Close()

	if err != nil {
		panic("Open config file failed!")
	}

	dec := json.NewDecoder(config)
	if err := dec.Decode(this); err != nil {
		panic("Config not a valid json format")
	}
}

func (this *KoaConfig) OpenLog() {
	// log file
	if this.LogFile != "stdout" {
		logFile, err := os.OpenFile(this.LogFile, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0660)
		if err != nil {
			panic("Open log file failed!")
		}
		log.SetOutput(logFile)
	}

	// Log formatter text|json
	switch this.LogFormat {
	case "text":
		log.SetFormatter(&log.TextFormatter{FullTimestamp: true})

	case "json":
		log.SetFormatter(&log.JSONFormatter{})
	default:
		panic("Invalid log format")
	}

	// log level
	switch this.LogLevel {
	case "debug":
		log.SetLevel(log.DebugLevel)
	case "info":
		log.SetLevel(log.InfoLevel)
	case "warn":
		log.SetLevel(log.WarnLevel)
	case "error":
		log.SetLevel(log.ErrorLevel)
	case "fatal":
		log.SetLevel(log.ErrorLevel)
	default:
		panic("Invalid log level")
	}
}
